<?php

namespace FeatureBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FeatureVersionAttachment
 *
 * @ORM\Table(name="feature_version_attachment", indexes={@ORM\Index(name="feature_version",
 *                                               columns={"feature_version"})})
 * @ORM\Entity
 */
class FeatureVersionAttachment {
    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255, nullable=false)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="filename", type="string", length=255, nullable=false)
     */
    private $filename;

    /**
     * @var string
     *
     * @ORM\Column(name="file_data", type="text", nullable=false)
     */
    private $fileData;

    /**
     * @var string
     *
     * @ORM\Column(name="version", type="string", length=255, nullable=true)
     */
    private $version;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \FeatureBundle\Entity\FeatureVersionStatus
     *
     * @ORM\ManyToOne(targetEntity="FeatureBundle\Entity\FeatureVersion")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="feature_version", referencedColumnName="id")
     * })
     */
    private $featureVersion;


    /**
     * Set type
     *
     * @param string $type
     *
     * @return FeatureVersionAttachment
     */
    public function setType($type) {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType() {
        return $this->type;
    }

    /**
     * Set filename
     *
     * @param string $filename
     *
     * @return FeatureVersionAttachment
     */
    public function setFilename($filename) {
        $this->filename = $filename;

        return $this;
    }

    /**
     * Get filename
     *
     * @return string
     */
    public function getFilename() {
        return $this->filename;
    }

    /**
     * Set fileData
     *
     * @param string $fileData
     *
     * @return FeatureVersionAttachment
     */
    public function setFileData($fileData) {
        $this->fileData = $fileData;

        return $this;
    }

    /**
     * Get fileData
     *
     * @return string
     */
    public function getFileData() {
        return $this->fileData;
    }

    /**
     * Set version
     *
     * @param string $version
     *
     * @return FeatureVersionAttachment
     */
    public function setVersion($version) {
        $this->version = $version;

        return $this;
    }

    /**
     * Get version
     *
     * @return string
     */
    public function getVersion() {
        return $this->version;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set featureVersion
     *
     * @param \FeatureBundle\Entity\FeatureVersionStatus $featureVersion
     *
     * @return FeatureVersionAttachment
     */
    public function setFeatureVersion(\FeatureBundle\Entity\FeatureVersionStatus $featureVersion = null) {
        $this->featureVersion = $featureVersion;

        return $this;
    }

    /**
     * Get featureVersion
     *
     * @return \FeatureBundle\Entity\FeatureVersionStatus
     */
    public function getFeatureVersion() {
        return $this->featureVersion;
    }
}
