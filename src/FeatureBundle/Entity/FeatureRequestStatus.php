<?php

namespace FeatureBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FeatureRequestStatus
 *
 * @ORM\Table(name="feature_request_status")
 * @ORM\Entity
 */
class FeatureRequestStatus {
    /**
     * @var string
     *
     * @ORM\Column(name="alias", type="string", length=255, nullable=false)
     */
    private $alias;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var boolean
     *
     * @ORM\Column(name="final", type="boolean", nullable=false)
     */
    private $final;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;


    /**
     * Set alias
     *
     * @param string $alias
     *
     * @return FeatureRequestStatus
     */
    public function setAlias($alias) {
        $this->alias = $alias;

        return $this;
    }

    /**
     * Get alias
     *
     * @return string
     */
    public function getAlias() {
        return $this->alias;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return FeatureRequestStatus
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set final
     *
     * @param boolean $final
     *
     * @return FeatureRequestStatus
     */
    public function setFinal($final) {
        $this->final = $final;

        return $this;
    }

    /**
     * Get final
     *
     * @return boolean
     */
    public function getFinal() {
        return $this->final;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }
}
