<?php

namespace FeatureBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Task
 *
 * @ORM\Table(name="task")
 * @ORM\Entity
 */
class Task {
    /**
     * @var integer
     *
     * @ORM\Column(name="external_id", type="integer", nullable=false)
     */
    private $externalId;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=false)
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="FeatureBundle\Entity\FeatureVersion", inversedBy="task")
     * @ORM\JoinTable(name="task_feature_version",
     *   joinColumns={
     *     @ORM\JoinColumn(name="task_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="feature_version_id", referencedColumnName="id")
     *   }
     * )
     */
    private $featureVersion;

    /**
     * Constructor
     */
    public function __construct() {
        $this->featureVersion = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Set externalId
     *
     * @param integer $externalId
     *
     * @return Task
     */
    public function setExternalId($externalId) {
        $this->externalId = $externalId;

        return $this;
    }

    /**
     * Get externalId
     *
     * @return integer
     */
    public function getExternalId() {
        return $this->externalId;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Task
     */
    public function setTitle($title) {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Task
     */
    public function setDescription($description) {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Add featureVersion
     *
     * @param \FeatureBundle\Entity\FeatureVersionStatus $featureVersion
     *
     * @return Task
     */
    public function addFeatureVersion(\FeatureBundle\Entity\FeatureVersionStatus $featureVersion) {
        $this->featureVersion[] = $featureVersion;

        return $this;
    }

    /**
     * Remove featureVersion
     *
     * @param \FeatureBundle\Entity\FeatureVersionStatus $featureVersion
     */
    public function removeFeatureVersion(\FeatureBundle\Entity\FeatureVersionStatus $featureVersion) {
        $this->featureVersion->removeElement($featureVersion);
    }

    /**
     * Get featureVersion
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFeatureVersion() {
        return $this->featureVersion;
    }
}
