<?php

namespace FeatureBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Product
 *
 * @ORM\Table(name="product")
 * @ORM\Entity
 * @ORM\EntityListeners({"FeatureBundle\Entity\Listener\ProductListener"})
 */
class Product {
    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=false)
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="FeatureBundle\Entity\Feature", inversedBy="product")
     * @ORM\JoinTable(name="product_feature",
     *   joinColumns={
     *     @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="feature_id", referencedColumnName="id")
     *   }
     * )
     */
    private $feature;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="FeatureBundle\Entity\ProductVersion", mappedBy="product")
     */
    private $versions;

    /**
     * Constructor
     */
    public function __construct() {
        $this->feature = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Set title
     *
     * @param string $title
     *
     * @return Product
     */
    public function setTitle($title) {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Product
     */
    public function setDescription($description) {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Add feature
     *
     * @param \FeatureBundle\Entity\Feature $feature
     *
     * @return Product
     */
    public function addFeature(\FeatureBundle\Entity\Feature $feature) {
        $this->feature[] = $feature;

        return $this;
    }

    /**
     * Remove feature
     *
     * @param \FeatureBundle\Entity\Feature $feature
     */
    public function removeFeature(\FeatureBundle\Entity\Feature $feature) {
        $this->feature->removeElement($feature);
    }

    /**
     * Get feature
     *
     * @return \Doctrine\ORM\PersistentCollection|Feature[]
     */
    public function getFeature() {
        return $this->feature;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection|ProductVersion[]
     */
    public function getVersions(){
        return $this->versions;
    }
}
