<?php

namespace FeatureBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * FeatureVersionProductVersionStatus
 *
 * @ORM\Table(name="feature_version_product_version_status", indexes={@ORM\Index(name="feature_version",
 *                                                           columns={"feature_version"}),
 * @ORM\Index(name="product_version", columns={"product_version"}),
 * @ORM\Index(name="fk_FVPVS_FVPSC", columns={"status"})})
 * @ORM\Entity(repositoryClass="FeatureBundle\Repository\FeatureVersionProductVersionStatusRepository")
 * @UniqueEntity(fields={"featureVersion", "productVersion"}, message="Связка версий продукта и фичи уже разерестрирована!")
 */
class FeatureVersionProductVersionStatus {
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \FeatureBundle\Entity\FeatureVersionStatus
     *
     * @ORM\ManyToOne(targetEntity="FeatureBundle\Entity\FeatureVersion")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="feature_version", referencedColumnName="id")
     * })
     */
    private $featureVersion;

    /**
     * @var \FeatureBundle\Entity\ProductVersion
     *
     * @ORM\ManyToOne(targetEntity="FeatureBundle\Entity\ProductVersion", inversedBy="featureVersionStatus")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="product_version", referencedColumnName="id")
     * })
     */
    private $productVersion;

    /**
     * @var \FeatureBundle\Entity\FeatureVersionProductVersionStatusChange
     *
     * @ORM\OneToOne(targetEntity="FeatureBundle\Entity\FeatureVersionProductVersionStatusChange", cascade={"persist", "remove"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="status", referencedColumnName="id", onDelete="CASCADE")
     * })
     */
    private $status;

    /**
     * @var \Doctrine\Common\Collections\Collection
     * @ORM\OneToMany(targetEntity="FeatureBundle\Entity\FeatureVersionProductVersionStatusChange", mappedBy="featureVersionProductVersionStatus")
     */
    private $statusChange;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set featureVersion
     *
     * @param FeatureVersion $featureVersion
     *
     * @return FeatureVersionProductVersionStatus
     */
    public function setFeatureVersion(FeatureVersion $featureVersion = null) {
        $this->featureVersion = $featureVersion;

        return $this;
    }

    /**
     * Get featureVersion
     *
     * @return \FeatureBundle\Entity\FeatureVersion
     */
    public function getFeatureVersion() {
        return $this->featureVersion;
    }

    /**
     * Set productVersion
     *
     * @param \FeatureBundle\Entity\ProductVersion $productVersion
     *
     * @return FeatureVersionProductVersionStatus
     */
    public function setProductVersion(\FeatureBundle\Entity\ProductVersion $productVersion = null) {
        $this->productVersion = $productVersion;

        return $this;
    }

    /**
     * Get productVersion
     *
     * @return \FeatureBundle\Entity\ProductVersion
     */
    public function getProductVersion() {
        return $this->productVersion;
    }

    /**
     * Set status
     *
     * @param FeatureVersionProductVersionStatusChange|FeatureVersionStatus $status
     *
     * @return FeatureVersionProductVersionStatus
     */
    public function setStatus($status = null) {
        $status = $status instanceof FeatureVersionProductVersionStatusChange
            ? $status
            : new FeatureVersionProductVersionStatusChange($status);
        $status->setFeatureVersionProductVersionStatus($this);
        $status->setPreviousChange($this->status);
        $this->status = $status;
        return $this;
    }

    /**
     * Get status
     *
     * @return \FeatureBundle\Entity\FeatureVersionProductVersionStatusChange
     */
    public function getStatus() {
        return $this->status;
    }


}
