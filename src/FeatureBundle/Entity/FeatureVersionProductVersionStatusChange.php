<?php

namespace FeatureBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FeatureVersionProductVersionStatusChange
 *
 * @ORM\Table(name="feature_version_product_version_status_change", indexes={@ORM\Index(name="fk_FVPSC_FVS",
 *                                                                  columns={"status"}),
 * @ORM\Index(name="fk_FVPSC_FVPSC", columns={"previous_change"}),
 * @ORM\Index(name="fk_FVPSC_FVPVS", columns={"feature_version_product_version_status"})})
 * @ORM\Entity(repositoryClass="FeatureBundle\Repository\FeatureVersionProductVersionStatusChangeRepository")
 */
class FeatureVersionProductVersionStatusChange {
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime", nullable=false)
     */
    private $date;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \FeatureBundle\Entity\FeatureVersionStatus
     *
     * @ORM\ManyToOne(targetEntity="FeatureBundle\Entity\FeatureVersionStatus")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="status", referencedColumnName="id")
     * })
     */
    private $status;

    /**
     * @var \FeatureBundle\Entity\FeatureVersionProductVersionStatusChange
     *
     * @ORM\OneToOne(targetEntity="FeatureBundle\Entity\FeatureVersionProductVersionStatusChange", inversedBy="nextChange", cascade={"remove", "persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="previous_change", referencedColumnName="id", onDelete="CASCADE")
     * })
     */
    private $previousChange;

    /**
     * @var \FeatureBundle\Entity\FeatureVersionProductVersionStatusChange
     *
     * @ORM\OneToOne(targetEntity="FeatureBundle\Entity\FeatureVersionProductVersionStatusChange", mappedBy="previousChange")
     */
    private $nextChange;

    /**
     * @var \FeatureBundle\Entity\FeatureVersionProductVersionStatus
     *
     * @ORM\ManyToOne(targetEntity="FeatureBundle\Entity\FeatureVersionProductVersionStatus", inversedBy="statusChange", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="feature_version_product_version_status", referencedColumnName="id", onDelete="CASCADE")
     * })
     */
    private $featureVersionProductVersionStatus;

    public function __construct(FeatureVersionStatus $status = null) {
        $this->status = $status;
        $this->date = new \DateTime('now', new \DateTimeZone('Europe/Moscow'));
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return FeatureVersionProductVersionStatusChange
     */
    public function setDate($date) {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate() {
        return $this->date;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set status
     *
     * @param \FeatureBundle\Entity\FeatureVersionStatus $status
     *
     * @return FeatureVersionProductVersionStatusChange
     */
    public function setStatus(\FeatureBundle\Entity\FeatureVersionStatus $status = null) {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return \FeatureBundle\Entity\FeatureVersionStatus
     */
    public function getStatus() {
        return $this->status;
    }

    /**
     * Set previousChange
     *
     * @param \FeatureBundle\Entity\FeatureVersionProductVersionStatusChange $previousChange
     *
     * @return FeatureVersionProductVersionStatusChange
     */
    public function setPreviousChange(\FeatureBundle\Entity\FeatureVersionProductVersionStatusChange $previousChange = null) {
        $this->previousChange = $previousChange;

        return $this;
    }

    /**
     * Get previousChange
     *
     * @return \FeatureBundle\Entity\FeatureVersionProductVersionStatusChange
     */
    public function getPreviousChange() {
        return $this->previousChange;
    }

    /**
     * Set featureVersionProductStatus
     *
     * @param \FeatureBundle\Entity\FeatureVersionProductVersionStatus $featureVersionProductVersionStatus
     *
     * @return FeatureVersionProductVersionStatusChange
     */
    public function setFeatureVersionProductVersionStatus(\FeatureBundle\Entity\FeatureVersionProductVersionStatus $featureVersionProductVersionStatus = null) {
        $this->featureVersionProductVersionStatus = $featureVersionProductVersionStatus;

        return $this;
    }

    /**
     * Get featureVersionProductStatus
     *
     * @return \FeatureBundle\Entity\FeatureVersionProductVersionStatus
     */
    public function getFeatureVersionProductVersionStatus() {
        return $this->featureVersionProductVersionStatus;
    }
}
