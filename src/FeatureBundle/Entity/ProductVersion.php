<?php

namespace FeatureBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * ProductVersion
 *
 * @ORM\Table(name="product_version", indexes={@ORM\Index(name="product", columns={"product"})})
 * @ORM\Entity
 * @UniqueEntity(fields={"product", "number"}, message="Версия уже зарегестрирована!")
 */
class ProductVersion {
    /**
     * @var string
     *
     * @Assert\Regex(pattern="/^(:?\d+\.?){2,}$/", message="Некорректный формат версии!")
     * @ORM\Column(name="number", type="string", length=255, nullable=false)
     */
    private $number;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=false)
     */
    private $description;

    /**
     * @var boolean
     *
     * @ORM\Column(name="current", type="boolean", nullable=true)
     */
    private $current;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \FeatureBundle\Entity\Product
     *
     * @ORM\ManyToOne(targetEntity="FeatureBundle\Entity\Product", inversedBy="versions")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="product", referencedColumnName="id")
     * })
     */
    private $product;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="FeatureBundle\Entity\FeatureVersionProductVersionStatus", mappedBy="productVersion")
     */
    private $featureVersionStatus;


    /**
     * Set number
     *
     * @param string $number
     *
     * @return ProductVersion
     */
    public function setNumber($number) {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return string
     */
    public function getNumber() {
        return $this->number;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return ProductVersion
     */
    public function setDescription($description) {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Set current
     *
     * @param boolean $current
     *
     * @return ProductVersion
     */
    public function setCurrent($current) {
        $this->current = $current;

        return $this;
    }

    /**
     * Get current
     *
     * @return boolean
     */
    public function getCurrent() {
        return $this->current;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set product
     *
     * @param \FeatureBundle\Entity\Product $product
     *
     * @return ProductVersion
     */
    public function setProduct(\FeatureBundle\Entity\Product $product = null) {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \FeatureBundle\Entity\Product
     */
    public function getProduct() {
        return $this->product;
    }

    public function getLabel() {
        return $this->product->getTitle() . ' v.' . $this->number;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFeatureVersionStatus() {
        return $this->featureVersionStatus;
    }
}
