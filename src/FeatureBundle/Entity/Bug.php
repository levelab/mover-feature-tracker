<?php

namespace FeatureBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Bug
 *
 * @ORM\Table(name="bug", indexes={@ORM\Index(name="product_version", columns={"product_version"})})
 * @ORM\Entity
 */
class Bug {
    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=false)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="location", type="text", nullable=true)
     */
    private $location;

    /**
     * @var string
     *
     * @ORM\Column(name="conditions", type="text", nullable=true)
     */
    private $conditions;

    /**
     * @var string
     *
     * @ORM\Column(name="expected_behavior", type="text", nullable=true)
     */
    private $expectedBehavior;

    /**
     * @var string
     *
     * @ORM\Column(name="how_to_reproduce", type="text", nullable=true)
     */
    private $howToReproduce;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="FeatureBundle\Entity\FeatureVersion", inversedBy="bugs")
     * @ORM\JoinTable(name="bug_feature_version",
     *   joinColumns={
     *     @ORM\JoinColumn(name="bug_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="feature_version_id", referencedColumnName="id")
     *   }
     * )
     */
    private $featureVersion;

    /**
     * @var \FeatureBundle\Entity\ProductVersion
     *
     * @ORM\ManyToOne(targetEntity="FeatureBundle\Entity\ProductVersion")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="product_version", referencedColumnName="id")
     * })
     */
    private $productVersion;


    /**
     * Set title
     *
     * @param string $title
     *
     * @return Bug
     */
    public function setTitle($title) {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Bug
     */
    public function setDescription($description) {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getLocation() {
        return $this->location;
    }

    /**
     * @param string $location
     *
     * @return Bug
     */
    public function setLocation($location) {
        $this->location = $location;

        return $this;
    }

    /**
     * @return string
     */
    public function getConditions() {
        return $this->conditions;
    }

    /**
     * @param string $conditions
     *
     * @return Bug
     */
    public function setConditions($conditions) {
        $this->conditions = $conditions;

        return $this;
    }

    /**
     * @return string
     */
    public function getExpectedBehavior() {
        return $this->expectedBehavior;
    }

    /**
     * @param string $expectedBehavior
     *
     * @return Bug
     */
    public function setExpectedBehavior($expectedBehavior) {
        $this->expectedBehavior = $expectedBehavior;

        return $this;
    }

    /**
     * @return string
     */
    public function getHowToReproduce() {
        return $this->howToReproduce;
    }

    /**
     * @param string $howToReproduce
     *
     * @return Bug
     */
    public function setHowToReproduce($howToReproduce) {
        $this->howToReproduce = $howToReproduce;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Add featureVersion
     *
     * @param \FeatureBundle\Entity\FeatureVersionStatus $featureVersion
     *
     * @return FeatureVersionStatus
     */
    public function addFeatureVersion(\FeatureBundle\Entity\FeatureVersionStatus $featureVersion) {
        $this->featureVersion[] = $featureVersion;

        return $this;
    }

    /**
     * Remove featureVersion
     *
     * @param \FeatureBundle\Entity\FeatureVersionStatus $featureVersion
     */
    public function removeFeatureVersion(\FeatureBundle\Entity\FeatureVersionStatus $featureVersion) {
        $this->featureVersion->removeElement($featureVersion);
    }

    /**
     * Get featureVersion
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFeatureVersion() {
        return $this->featureVersion;
    }

    /**
     * Set productVersion
     *
     * @param \FeatureBundle\Entity\ProductVersion $productVersion
     *
     * @return Bug
     */
    public function setProductVersion(\FeatureBundle\Entity\ProductVersion $productVersion = null) {
        $this->productVersion = $productVersion;

        return $this;
    }

    /**
     * Get productVersion
     *
     * @return \FeatureBundle\Entity\ProductVersion
     */
    public function getProductVersion() {
        return $this->productVersion;
    }
    
    
}
