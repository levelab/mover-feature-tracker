<?php

namespace FeatureBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FeatureRequest
 *
 * @ORM\Table(name="feature_request", indexes={@ORM\Index(name="status", columns={"status"}),
 * @ORM\Index(name="fk_request_product_1", columns={"product_id"})})
 * @ORM\Entity
 */
class FeatureRequest {
    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=false)
     */
    private $description;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \FeatureBundle\Entity\FeatureRequestStatusChange
     *
     * @ORM\ManyToOne(targetEntity="FeatureBundle\Entity\FeatureRequestStatusChange", cascade={"persist", "remove"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="status", referencedColumnName="id")
     * })
     */
    private $status;

    /**
     * @var \FeatureBundle\Entity\Product
     *
     * @ORM\ManyToOne(targetEntity="FeatureBundle\Entity\Product")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     * })
     */
    private $product;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="FeatureBundle\Entity\Feature", inversedBy="featureRequest")
     * @ORM\JoinTable(name="feature_request_feature",
     *   joinColumns={
     *     @ORM\JoinColumn(name="feature_request_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="feature_id", referencedColumnName="id")
     *   }
     * )
     */
    private $features;

    /**
     * @var \FeatureBundle\Entity\Feature
     * @ORM\OneToOne(targetEntity="\FeatureBundle\Entity\Feature")
     * @ORM\JoinColumn(name="created_feature_id", referencedColumnName="id")
     */
    private $createdFeature;

    /**
     * Constructor
     */
    public function __construct() {
        $this->created = new \DateTime('now', new \DateTimeZone('Europe/Moscow'));
        $this->features = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return FeatureRequest
     */
    public function setTitle($title) {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return FeatureRequest
     */
    public function setDescription($description) {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return FeatureRequest
     */
    public function setCreated($created) {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated() {
        return $this->created;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set status
     *
     * @param FeatureRequestStatusChange|FeatureRequestStatus $status
     *
     * @return FeatureRequest
     */
    public function setStatus($status = null) {
        $status = $status instanceof FeatureRequestStatusChange
            ? $status
            : new FeatureRequestStatusChange($status);
        $status->setFeatureRequest($this);
        $status->setPreviousStatus($this->status);
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return \FeatureBundle\Entity\FeatureRequestStatusChange
     */
    public function getStatus() {
        return $this->status;
    }

    /**
     * Set product
     *
     * @param \FeatureBundle\Entity\Product $product
     *
     * @return FeatureRequest
     */
    public function setProduct(\FeatureBundle\Entity\Product $product = null) {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \FeatureBundle\Entity\Product
     */
    public function getProduct() {
        return $this->product;
    }

    /**
     * Add feature
     *
     * @param \FeatureBundle\Entity\Feature $feature
     *
     * @return FeatureRequest
     */
    public function addFeature(\FeatureBundle\Entity\Feature $feature) {
        $this->features[] = $feature;

        return $this;
    }

    /**
     * Remove feature
     *
     * @param \FeatureBundle\Entity\Feature $feature
     */
    public function removeFeature(\FeatureBundle\Entity\Feature $feature) {
        $this->features->removeElement($feature);
    }

    /**
     * Get feature
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFeatures() {
        return $this->features;
    }

    /**
     * @return Feature
     */
    public function getCreatedFeature() {
        return $this->createdFeature;
    }

    /**
     * @param Feature $createdFeature
     *
     * @return FeatureRequest
     */
    public function setCreatedFeature($createdFeature) {
        $this->createdFeature = $createdFeature;
        return $this;
    }
}
