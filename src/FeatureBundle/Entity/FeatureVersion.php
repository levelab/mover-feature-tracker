<?php

namespace FeatureBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * FeatureVersion
 *
 * @ORM\Table(name="feature_version", indexes={@ORM\Index(name="feature", columns={"feature"}),
 * @ORM\Index(name="fk_FV_FR", columns={"request_id"})})
 * @UniqueEntity(fields={"feature", "number"}, message="Версия уже зарегестрирована!")
 * @ORM\Entity
 */
class FeatureVersion {
    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @Assert\Regex(pattern="/^(:?\d+\.?){2,}$/", message="Некорректный формат версии!")
     * @ORM\Column(name="number", type="string", length=255, nullable=false)
     */
    private $number;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \FeatureBundle\Entity\FeatureRequest
     *
     * @ORM\ManyToOne(targetEntity="FeatureBundle\Entity\FeatureRequest")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="request_id", referencedColumnName="id")
     * })
     */
    private $request;

    /**
     * @var \FeatureBundle\Entity\Feature
     *
     * @ORM\ManyToOne(targetEntity="FeatureBundle\Entity\Feature")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="feature", referencedColumnName="id")
     * })
     */
    private $feature;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="FeatureBundle\Entity\Task", mappedBy="featureVersion")
     */
    private $task;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="FeatureBundle\Entity\Bug", mappedBy="featureVersion")
     */
    private $bug;


    /**
     * Constructor
     */
    public function __construct() {
        $this->task = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Set description
     *
     * @param string $description
     *
     * @return FeatureVersionStatus
     */
    public function setDescription($description) {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Set number
     *
     * @param string $number
     *
     * @return FeatureVersionStatus
     */
    public function setNumber($number) {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return string
     */
    public function getNumber() {
        return $this->number;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set request
     *
     * @param \FeatureBundle\Entity\FeatureRequest $request
     *
     * @return FeatureVersionStatus
     */
    public function setRequest(\FeatureBundle\Entity\FeatureRequest $request = null) {
        $this->request = $request;

        return $this;
    }

    /**
     * Get request
     *
     * @return \FeatureBundle\Entity\FeatureRequest
     */
    public function getRequest() {
        return $this->request;
    }

    /**
     * Set feature
     *
     * @param \FeatureBundle\Entity\Feature $feature
     *
     * @return FeatureVersionStatus
     */
    public function setFeature(\FeatureBundle\Entity\Feature $feature = null) {
        $this->feature = $feature;

        return $this;
    }

    /**
     * Get feature
     *
     * @return \FeatureBundle\Entity\Feature
     */
    public function getFeature() {
        return $this->feature;
    }

    /**
     * Add task
     *
     * @param \FeatureBundle\Entity\Task $task
     *
     * @return FeatureVersionStatus
     */
    public function addTask(\FeatureBundle\Entity\Task $task) {
        $this->task[] = $task;

        return $this;
    }

    /**
     * Remove task
     *
     * @param \FeatureBundle\Entity\Task $task
     */
    public function removeTask(\FeatureBundle\Entity\Task $task) {
        $this->task->removeElement($task);
    }

    /**
     * Get task
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTask() {
        return $this->task;
    } 
    
    /**
     * Add bug
     *
     * @param \FeatureBundle\Entity\Bug $bug
     *
     * @return FeatureVersionStatus
     */
    public function addBug(\FeatureBundle\Entity\Bug $bug) {
        $this->bug[] = $bug;

        return $this;
    }

    /**
     * Remove bug
     *
     * @param \FeatureBundle\Entity\Bug $bug
     */
    public function removeBug(\FeatureBundle\Entity\Bug $bug) {
        $this->bug->removeElement($bug);
    }

    /**
     * Get bug
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBug() {
        return $this->bug;
    }

    public function getLabel() {
        return $this->feature->getTitle() . ' v.' . $this->number;
    }
}
