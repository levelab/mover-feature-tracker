<?php

namespace FeatureBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FeatureRequestStatusChange
 *
 * @ORM\Table(name="feature_request_status_change", indexes={@ORM\Index(name="fk_feature_request_status_change_feature_request_status_1",
 *                                                  columns={"status"}),
 * @ORM\Index(name="fk_feature_request_status_change_feature_request_status_change_1", columns={"previous_status"}),
 * @ORM\Index(name="fk_feature_request_status_change_feature_request_1", columns={"feature_request"})})
 * @ORM\Entity
 */
class FeatureRequestStatusChange {
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime", nullable=false)
     */
    private $date;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \FeatureBundle\Entity\FeatureRequestStatusChange
     *
     * @ORM\ManyToOne(targetEntity="FeatureBundle\Entity\FeatureRequestStatusChange")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="previous_status", referencedColumnName="id")
     * })
     */
    private $previousStatus;

    /**
     * @var \FeatureBundle\Entity\FeatureRequestStatus
     *
     * @ORM\ManyToOne(targetEntity="FeatureBundle\Entity\FeatureRequestStatus")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="status", referencedColumnName="id")
     * })
     */
    private $status;

    /**
     * @var \FeatureBundle\Entity\FeatureRequest
     *
     * @ORM\ManyToOne(targetEntity="FeatureBundle\Entity\FeatureRequest")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="feature_request", referencedColumnName="id")
     * })
     */
    private $featureRequest;

    public function __construct(FeatureRequestStatus $status = null) {
        $this->status = $status;
        $this->date = new \DateTime('now', new \DateTimeZone('Europe/Moscow'));
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return FeatureRequestStatusChange
     */
    public function setDate($date) {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate() {
        return $this->date;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set previousStatus
     *
     * @param \FeatureBundle\Entity\FeatureRequestStatusChange $previousStatus
     *
     * @return FeatureRequestStatusChange
     */
    public function setPreviousStatus(\FeatureBundle\Entity\FeatureRequestStatusChange $previousStatus = null) {
        $this->previousStatus = $previousStatus;

        return $this;
    }

    /**
     * Get previousStatus
     *
     * @return \FeatureBundle\Entity\FeatureRequestStatusChange
     */
    public function getPreviousStatus() {
        return $this->previousStatus;
    }

    /**
     * Set status
     *
     * @param \FeatureBundle\Entity\FeatureRequestStatus $status
     *
     * @return FeatureRequestStatusChange
     */
    public function setStatus(\FeatureBundle\Entity\FeatureRequestStatus $status = null) {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return \FeatureBundle\Entity\FeatureRequestStatus
     */
    public function getStatus() {
        return $this->status;
    }

    /**
     * Set featureRequest
     *
     * @param \FeatureBundle\Entity\FeatureRequest $featureRequest
     *
     * @return FeatureRequestStatusChange
     */
    public function setFeatureRequest(\FeatureBundle\Entity\FeatureRequest $featureRequest = null) {
        $this->featureRequest = $featureRequest;

        return $this;
    }

    /**
     * Get featureRequest
     *
     * @return \FeatureBundle\Entity\FeatureRequest
     */
    public function getFeatureRequest() {
        return $this->featureRequest;
    }
}
