<?php

namespace FeatureBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Feature
 *
 * @ORM\Table(name="feature", indexes={@ORM\Index(name="request", columns={"request_id"})})
 * @ORM\Entity
 * @ORM\EntityListeners({"FeatureBundle\Entity\Listener\FeatureListener"})
 */
class Feature {
    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=false)
     */
    private $description;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \FeatureBundle\Entity\FeatureRequest
     *
     * @ORM\ManyToOne(targetEntity="FeatureBundle\Entity\FeatureRequest")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="request_id", referencedColumnName="id")
     * })
     */
    private $request;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="FeatureBundle\Entity\Product", mappedBy="feature")
     */
    private $product;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="FeatureBundle\Entity\FeatureRequest", mappedBy="createdFeature")
     */
    private $featureRequest;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="FeatureBundle\Entity\FeatureVersion", mappedBy="feature")
     */
    private $versions;

    /**
     * Constructor
     */
    public function __construct() {
        $this->created = new \DateTime('now', new \DateTimeZone('Europe/Moscow'));
        $this->product = new \Doctrine\Common\Collections\ArrayCollection();
        $this->featureRequest = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Set title
     *
     * @param string $title
     *
     * @return Feature
     */
    public function setTitle($title) {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Feature
     */
    public function setDescription($description) {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return Feature
     */
    public function setCreated($created) {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated() {
        return $this->created;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set request
     *
     * @param \FeatureBundle\Entity\FeatureRequest $request
     *
     * @return Feature
     */
    public function setRequest(\FeatureBundle\Entity\FeatureRequest $request = null) {
        $this->request = $request;

        return $this;
    }

    /**
     * Get request
     *
     * @return \FeatureBundle\Entity\FeatureRequest
     */
    public function getRequest() {
        return $this->request;
    }

    /**
     * Add product
     *
     * @param \FeatureBundle\Entity\Product $product
     *
     * @return Feature
     */
    public function addProduct(\FeatureBundle\Entity\Product $product) {
        $this->product[] = $product;
        $product->addFeature($this);
        return $this;
    }

    /**
     * Remove product
     *
     * @param \FeatureBundle\Entity\Product $product
     */
    public function removeProduct(\FeatureBundle\Entity\Product $product) {
        $this->product->removeElement($product);
        $product->removeFeature($this);
    }

    /**
     * Get product
     *
     * @return \Doctrine\ORM\PersistentCollection
     */
    public function getProduct() {
        return $this->product;
    }

    /**
     * Add featureRequest
     *
     * @param \FeatureBundle\Entity\FeatureRequest $featureRequest
     *
     * @return Feature
     */
    public function addFeatureRequest(\FeatureBundle\Entity\FeatureRequest $featureRequest) {
        $this->featureRequest[] = $featureRequest;

        return $this;
    }

    /**
     * Remove featureRequest
     *
     * @param \FeatureBundle\Entity\FeatureRequest $featureRequest
     */
    public function removeFeatureRequest(\FeatureBundle\Entity\FeatureRequest $featureRequest) {
        $this->featureRequest->removeElement($featureRequest);
    }

    /**
     * Get featureRequest
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFeatureRequest() {
        return $this->featureRequest;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVersions() {
        return $this->versions;
    }
}
