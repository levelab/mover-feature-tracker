<?php
namespace FeatureBundle\Entity\Listener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\OnFlushEventArgs;
use FeatureBundle\Entity\Feature;
use FeatureBundle\Entity\FeatureVersionStatus;
use FeatureBundle\Entity\FeatureVersionProductVersionStatus;
use FeatureBundle\Entity\FeatureVersionProductVersionStatusChange;
use FeatureBundle\Entity\Product;
use FeatureBundle\Entity\ProductVersion;

class ProductListener {
    /** @var FeatureVersionProductVersionStatus[] $statusesToCreate */
    protected $statusesToCreate = [];

    /** @var FeatureVersionProductVersionStatus[] $statusesToDelete */
    protected $statusesToDelete = [];

    /** @var FeatureVersionStatus $featureVersionStartStatus */
    protected $featureVersionStartStatus;

    public function preUpdate($entity, LifecycleEventArgs $event) {
        $em = $event->getEntityManager();

        /** @var Product $entity */
        /** @var Feature[] $deletedFeatures */
        $deletedFeatures = $entity->getFeature()->getDeleteDiff();
        if(count($deletedFeatures)) {
            $entity->getVersions()->map(function (ProductVersion $productVersion) use ($deletedFeatures) {
                array_map(function (Feature $feature) use ($productVersion) {
                    $featureVersions = $feature->getVersions()->toArray();
                    $this->statusesToDelete = array_merge($this->statusesToDelete,
                        $productVersion->getFeatureVersionStatus()->filter(function (FeatureVersionProductVersionStatus $status) use ($featureVersions) {
                            return in_array($status->getFeatureVersion(), $featureVersions);
                        })->toArray());
                }, $deletedFeatures);
            });
        }

        /** @var Feature[] $addedFeatures */
        $addedFeatures = $entity->getFeature()->getInsertDiff();
        if(count($addedFeatures)) {
            $this->featureVersionStartStatus = $em->getRepository('FeatureBundle:FeatureVersionStatus')->find(1);
            $entity->getVersions()->map(function (ProductVersion $productVersion) use ($addedFeatures) {
                array_map(function (Feature $feature) use ($productVersion) {
                    $feature->getVersions()->map(function (FeatureVersionStatus $featureVersion) use ($productVersion) {
                        $initialStatusChange = new FeatureVersionProductVersionStatusChange($this->featureVersionStartStatus);
                        $featureVersionProductVersionStatus = new FeatureVersionProductVersionStatus();
                        $featureVersionProductVersionStatus->setStatus($initialStatusChange);
                        $featureVersionProductVersionStatus->setFeatureVersion($featureVersion);
                        $featureVersionProductVersionStatus->setProductVersion($productVersion);
                        $this->statusesToCreate[] = $featureVersionProductVersionStatus;
                    });
                }, $addedFeatures);
            });
        }
    }

    public function postUpdate($entity, LifecycleEventArgs $event) {
        if(empty($this->statusesToDelete) && empty($this->statusesToCreate)) {
            return;
        }
        $em = $event->getEntityManager();
        array_map(function ($status) use ($em) {
            $em->persist($status);
        }, $this->statusesToCreate);
        array_map(function (FeatureVersionProductVersionStatus $status) use ($em) {
            $status->setStatus(null);
            $em->remove($status);
        }, $this->statusesToDelete);
        $em->flush();
    }
}