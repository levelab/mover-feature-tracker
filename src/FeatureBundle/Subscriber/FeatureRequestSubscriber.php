<?php

namespace FeatureBundle\Subscriber;

use FeatureBundle\Event\FeatureRequestCreateEvent;
use FeatureBundle\Service\FeatureRequestService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class FeatureRequestSubscriber implements EventSubscriberInterface {
    /** @var FeatureRequestService */
    protected $service;

    public function __construct(FeatureRequestService $service) {
        $this->service = $service;
    }

    public static function getSubscribedEvents() {
        return array(
            FeatureRequestCreateEvent::NAME => array(
                [
                    'onCreate',
                    900
                ]
            )
        );
    }

    public function onCreate(FeatureRequestCreateEvent $event) {
        $this->service->persist($event->getFeatureRequest());
    }
}