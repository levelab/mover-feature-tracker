<?php

namespace FeatureBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Serializer;

class DefaultController extends Controller {
    /**
     * @Route("/")
     */
    public function indexAction() {
        return $this->render('FeatureBundle:Default:index.html.twig');
    }

    /**
     * @return EventDispatcher
     */
    protected function getEventDispatcher() {
        return $this->get('event_dispatcher');
    }

    protected function createView($data) {
        /** @var Serializer $serializer */
        $serializer = $this->get('serializer');
        $serializedData = $serializer->normalize($data, 'json');
        return new JsonResponse($serializedData);
    }
}
