<?php

namespace FeatureBundle\Controller;

use FeatureBundle\Entity\Product;
use FeatureBundle\Service\FeatureVersionProductVersionStatusChangeService;
use FeatureBundle\Service\FeatureVersionProductVersionStatusService;
//use Sonata\AdminBundle\Controller\CRUDController as Controller;
use FeatureBundle\Service\FeatureVersionStatusService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class ReportController extends Controller {

    /**
     * @Route ("/feature_versions_for_product_versions_list", name="feature_versions_for_product_versions_list",
     *        methods={"GET"})
     */
    public function fullListAction() {
        /** @var FeatureVersionProductVersionStatusService $service */
        $service = $this->get('feature.feature_version_product_version_status');
        $list = $service->getFullList();
        return $this->render('FeatureBundle:Admin:report/feature_versions_for_product_versions.html.twig', [
            'list' => $list
        ]);
    }

    /**
     * @Route ("/feature_versions_status_changes_for_product/{product}",
     *        name="feature_versions_status_changes_for_product", methods={"GET"})
     */
    public function statusChangeAction(Product $product) {
        /** @var FeatureVersionStatusService $statusService */
        $statusService  = $this->get('feature.feature_version_status');
        $statusDictionary = $statusService->getAll();

        /** @var FeatureVersionProductVersionStatusChangeService $changesService */
        $changesService = $this->get('feature.feature_version_product_version_status_change');
        $changes = $changesService->getByProduct($product);
        //var_dump($list);
        return $this->render('FeatureBundle:Admin:report/feature_versions_status_changes_for_product.html.twig', [
            'product' => $product,
            'changes' => $changes,
            'statuses' => $statusDictionary
        ]);
    }
}