<?php

namespace FeatureBundle\Controller;


use FeatureBundle\Entity\FeatureRequest;
use FeatureBundle\Event\FeatureRequestCreateEvent;
use FeatureBundle\Form\RequestType;
use FeatureBundle\Service\FeatureRequestService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class RequestController extends DefaultController {
    /**
     * @return FeatureRequestService
     */
    protected function getRequestService() {
        return $this->get('feature.feature_request');
    }

    /**
     * @Route("/feature_request", name = "feature_request_list", methods={"GET"})
     */
    public function indexAction() {
        $requests = $this->getRequestService()->getList();
        return $this->createView($requests);
    }

    /**
     * @Route("/feature_request/create", name = "feature_request_create")
     */
    public function createRequestFormAction(Request $request) {
        $featureRequest = new FeatureRequest();
        $form = $this->createForm(RequestType::class, $featureRequest, [
            'action' => $this->generateUrl('feature_request_create'),
            'method' => 'POST',
        ]);

        if($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            if($form->isValid()) {
                $event = new FeatureRequestCreateEvent($featureRequest);
                $this->getEventDispatcher()->dispatch(FeatureRequestCreateEvent::NAME, $event);
            }
        }
        return $this->render('FeatureBundle:Form:request.html.twig', array(
            'form' => $form->createView(),
        ));
    }


    /**
     * @Route("/feature_request/{featureRequest}", name = "feature_request_view", methods={"GET"})
     * @param FeatureRequest $featureRequest
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function getRequestAction(FeatureRequest $featureRequest) {
        return $this->createView($featureRequest);
    }

    /**
     * @Route("/feature_request/{featureRequest}/edit", name = "feature_request_edit_form", methods={"GET"})
     */
    public function editRequestFormAction() {

    }
}