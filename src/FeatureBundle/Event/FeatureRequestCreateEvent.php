<?php

namespace FeatureBundle\Event;

use FeatureBundle\Entity\FeatureRequest;
use Symfony\Component\EventDispatcher\Event;

class FeatureRequestCreateEvent extends Event {
    const NAME = 'feature_request.create';

    /** @var  FeatureRequest $featureRequest */
    protected $featureRequest;

    /**
     * @return FeatureRequest
     */
    public function getFeatureRequest() {
        return $this->featureRequest;
    }

    public function __construct(FeatureRequest $featureRequest) {
        $this->featureRequest = $featureRequest;
    }
}