<?php

namespace FeatureBundle\Repository;


use Doctrine\ORM\EntityRepository;
use FeatureBundle\Entity\Product;

class FeatureVersionProductVersionStatusChangeRepository extends EntityRepository {
    public function getChangesByProduct(Product $product) {
        $qb = $this->createQueryBuilder('sc')
            ->select([
                'pv.number as product_version',
                'f.title as feature_name',
                'fv.number as feature_version_number',
                'IDENTITY(sc.status) as feature_version_status_id',
                'sc.date as start_date',
                'next.date as end_date',
                'pv.id as product_version_id',
                'fv.id as feature_version_id',
            ])
            ->join('sc.featureVersionProductVersionStatus', 'fvpvs')
            ->join('fvpvs.featureVersion', 'fv')
            ->join('fvpvs.productVersion', 'pv')
            ->join('fv.feature', 'f')
            ->leftJoin('sc.nextChange', 'next')
            ->where('pv.product = :product')
            ->setParameter('product', $product);

        return $qb->getQuery()->getArrayResult();
    }
}