<?php

namespace FeatureBundle\Repository;


use Doctrine\ORM\EntityRepository;

class DefaultRepository extends EntityRepository {

    public function save($object) {
        $this->getEntityManager()->persist($object);
        $this->getEntityManager()->flush($object);
    }
}