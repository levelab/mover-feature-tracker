<?php

namespace FeatureBundle\Repository;

use Doctrine\ORM\EntityRepository;

class FeatureVersionProductVersionStatusRepository extends EntityRepository {

    public function getVersionsReport() {
        $qb = $this->createQueryBuilder('fvpvs')
            ->select([
                'p.id as product_id',
                'p.title as product_name',
                'pv.number as product_version',
                'f.id as feature_id',
                'f.title as feature_name',
                'fv.id as feature_version_id',
                'fv.number as feature_version_number',
                'fvpvs.id as feature_version_status_id',
                's.name as feature_version_status_name',
            ])
            ->join('fvpvs.featureVersion', 'fv')
            ->join('fvpvs.productVersion', 'pv')
            ->join('fvpvs.status', 'sc')
            ->join('sc.status', 's')
            ->join('fv.feature', 'f')
            ->join('pv.product', 'p');

        return $qb->getQuery()->getArrayResult();
    }
}