<?php

namespace FeatureBundle\Service;


use FeatureBundle\Entity\Product;
use FeatureBundle\Repository\FeatureVersionProductVersionStatusChangeRepository;

class FeatureVersionProductVersionStatusChangeService {
    /** @var FeatureVersionProductVersionStatusChangeRepository */
    protected $repository;

    public function __construct(FeatureVersionProductVersionStatusChangeRepository $repository) {
        $this->repository = $repository;
    }

    public function getByProduct(Product $product) {
        $changes = $this->repository->getChangesByProduct($product);
        $result = [];
        $now = new \DateTime('now', new \DateTimeZone('Europe/Moscow'));
        foreach($changes as $change) {
            empty($result[$change['product_version_id']]) && $result[$change['product_version_id']] = [
                'label' => $product->getTitle() . ' v.' . $change['product_version'],
                'features_versions' => []
            ];
            empty($result[$change['product_version_id']]['features_versions'][$change['feature_version_id']]) && $result[$change['product_version_id']]['features_versions'][$change['feature_version_id']] = [
                'label' => $change['feature_name'] . ' v.' . $change['feature_version_number'],
                'status_changes' => []
            ];
            $result[$change['product_version_id']]['features_versions'][$change['feature_version_id']]['status_changes'][$change['feature_version_status_id']] =
                $change['end_date']
                    ? $change['end_date']->diff($change['start_date'])->format('%d:%h:%i')
                    : $now->diff($change['start_date'])->format('%d:%h:%i') . '+';
        }

        return $result;
    }
}