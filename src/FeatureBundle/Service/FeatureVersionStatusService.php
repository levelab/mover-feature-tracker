<?php

namespace FeatureBundle\Service;


use FeatureBundle\Repository\FeatureVersionStatusRepository;

class FeatureVersionStatusService {
    /** @var FeatureVersionStatusRepository */
    protected $repository;

    public function __construct(FeatureVersionStatusRepository $repository) {
        $this->repository = $repository;
    }

    public function getAll() {
        return $this->repository->findAll();
    }
}