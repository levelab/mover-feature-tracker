<?php

namespace FeatureBundle\Service;


use FeatureBundle\Entity\FeatureRequest;
use FeatureBundle\Repository\FeatureRequestRepository;

class FeatureRequestService {

    /** @var FeatureRequestRepository */
    protected $repository;

    public function __construct(FeatureRequestRepository $repository) {
        $this->repository = $repository;
    }

    public function getList() {
        return $this->repository->findAll();
    }

    public function persist(FeatureRequest $request) {
        $this->repository->save($request);
    }
}