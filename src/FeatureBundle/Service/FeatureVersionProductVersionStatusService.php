<?php

namespace FeatureBundle\Service;


use FeatureBundle\Entity\FeatureVersionProductVersionStatus;
use FeatureBundle\Repository\FeatureVersionProductVersionStatusRepository;

class FeatureVersionProductVersionStatusService {
    /** @var FeatureVersionProductVersionStatusRepository */
    protected $repository;

    public function __construct(FeatureVersionProductVersionStatusRepository $repository) {
        $this->repository = $repository;
    }

    public function getFullList() {
        $result = [];
        /** @var FeatureVersionProductVersionStatus[] $statuses */
        $statuses = $this->repository->getVersionsReport();;
        foreach($statuses as $status) {
            $productLabel = $status['product_id'] . '|||' . $status['product_name'];
            $productVersionLabel = $status['product_name'] . ' v.' . $status['product_version'];
            empty($result[$productLabel]) && $result[$productLabel] = [];
            empty($result[$productLabel][$productVersionLabel]) && $result[$productLabel][$productVersionLabel] = [];
            $result[$productLabel][$productVersionLabel][] = [
                'feature_id' => $status['feature_id'],
                'feature_name' => $status['feature_name'],
                'feature_version_id' => $status['feature_version_id'],
                'feature_version_number' => $status['feature_version_number'],
                'feature_version_status_id' => $status['feature_version_status_id'],
                'feature_version_status_name' => $status['feature_version_status_name']
            ];
        }
        return $result;
    }
}