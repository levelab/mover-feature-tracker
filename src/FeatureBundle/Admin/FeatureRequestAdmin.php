<?php

namespace FeatureBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class FeatureRequestAdmin extends AbstractAdmin {
    protected function configureFormFields(FormMapper $formMapper) {
        $status = $this->getSubject()->getId()
            ? $this->getSubject()->getStatus()->getStatus()
            : null;

        $formMapper
            ->add('title', 'text')
            ->add('description', 'textarea')
            ->add('status', 'entity', array(
                'class' => 'FeatureBundle\Entity\FeatureRequestStatus',
                'choice_label' => 'name',
                'data' => $status
            ))
            ->add('product', 'sonata_type_model', array(
                'class' => 'FeatureBundle\Entity\Product',
                'property' => 'title',
                'required' => false
            ))
            ->add('createdFeature', 'sonata_type_model', array(
                'class' => 'FeatureBundle\Entity\Feature',
                'property' => 'title',
                'required' => false
            ));
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
            ->add('title')
            ->add('status.status.name')
            ->add('product.title');
    }

    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
            ->addIdentifier('title')
            ->addIdentifier('status.status.name')
            ->addIdentifier('product.title');
    }
}