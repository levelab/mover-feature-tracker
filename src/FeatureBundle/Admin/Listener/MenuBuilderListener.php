<?php

namespace FeatureBundle\Admin\Listener;

use Sonata\AdminBundle\Event\ConfigureMenuEvent;

class MenuBuilderListener {
    public function addMenuItems(ConfigureMenuEvent $event)
    {
        $menu = $event->getMenu();

        $folder =  $menu
            ->addChild('reports');
        $folder->setLabel('Reports');
        $child =$folder
            ->addChild('Версии фичей по версиям продуктов', array(
                'route' => 'feature_versions_for_product_versions_list'
            ));


        $child->setLabel('Версии фичей по версиям продуктов');
    }
}