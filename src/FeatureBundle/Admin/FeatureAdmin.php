<?php

namespace FeatureBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class FeatureAdmin extends AbstractAdmin {
    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
            ->add('title', 'text')
            ->add('description', 'textarea')
            ->add('request', 'sonata_type_model', array(
                'class' => 'FeatureBundle\Entity\FeatureRequest',
                'property' => 'title',
                'required' => false
            ))
             ->add('product', 'sonata_type_model', [
                 'by_reference' => false,
                 'property' => 'title',
                 'required' => false,
                 'multiple' => true
             ]);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
            ->add('title')
            ->add('request.title');
    }

    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
            ->addIdentifier('title')
            ->addIdentifier('request.title');
    }
}