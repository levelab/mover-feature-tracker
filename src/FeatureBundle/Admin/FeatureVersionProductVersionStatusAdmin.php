<?php

namespace FeatureBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class FeatureVersionProductVersionStatusAdmin extends AbstractAdmin {
    protected function configureFormFields(FormMapper $formMapper) {
        $status = $this->getSubject()->getStatus()
            ? $this->getSubject()->getStatus()->getStatus()
            : null;

        $formMapper
            ->add('status', 'entity', array(
                'class' => 'FeatureBundle\Entity\FeatureVersionStatus',
                'choice_label' => 'name',
                'data' => $status
            ))
            ->add('featureVersion', 'sonata_type_model', array(
                'class' => 'FeatureBundle\Entity\FeatureVersion',
                'property' => 'label'
            ))
            ->add('productVersion', 'sonata_type_model', array(
                'class' => 'FeatureBundle\Entity\ProductVersion',
                'property' => 'label'
            ));
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
            ->add('status.status.name');
    }

    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
            ->addIdentifier('featureVersion', null, ['associated_property' => 'label'])
            ->addIdentifier('productVersion', null, ['associated_property' => 'label'])
            ->addIdentifier('status.status.name');
    }
}