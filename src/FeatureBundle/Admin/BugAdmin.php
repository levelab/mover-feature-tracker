<?php

namespace FeatureBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class BugAdmin extends AbstractAdmin {
    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
            ->add('title', 'text')
            ->add('description', 'textarea')
            ->add('location', 'textarea')
            ->add('conditions', 'textarea')
            ->add('expectedBehavior', 'textarea')
            ->add('howToReproduce', 'textarea')
            ->add('featureVersion', 'sonata_type_model', array(
                'class' => 'FeatureBundle\Entity\FeatureVersion',
                'property' => 'label',
                'required' => false
            ))
            ->add('productVersion', 'sonata_type_model', array(
                'class' => 'FeatureBundle\Entity\ProductVersion',
                'property' => 'label',
                'required' => false
            ))
            ->add('featureVersion', 'sonata_type_model', [
                'multiple' => true,
                'by_reference' => false,
                'property' => 'label',
                'required' => false
            ]);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
            ->add('title');
    }

    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
            ->addIdentifier('productVersion', null, ['associated_property' => 'label'])
            ->addIdentifier('title')
            ->addIdentifier('description');
    }
}