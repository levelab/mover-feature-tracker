<?php

namespace FeatureBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class FeatureVersionAdmin extends AbstractAdmin {
    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
            ->add('number', 'text')
            ->add('description', 'textarea')
            ->add('feature', 'sonata_type_model', array(
                'class' => 'FeatureBundle\Entity\Feature',
                'property' => 'title'
            ))
            ->add('request', 'sonata_type_model', array(
                'class' => 'FeatureBundle\Entity\FeatureRequest',
                'property' => 'title',
                'required' => false
            ));
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
            ->add('feature.title')
            ->add('number');
    }

    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
            ->addIdentifier('feature.title')
            ->addIdentifier('number');
    }
}