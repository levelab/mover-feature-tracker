<?php

namespace FeatureBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
class ProductVersionAdmin extends AbstractAdmin{
    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
            ->add('number', 'text')
            ->add('description', 'textarea')
            ->add('product', 'sonata_type_model', array(
                'class' => 'FeatureBundle\Entity\Product',
                'property' => 'title'
            ))->add('current', 'checkbox', [
                'required' => false
            ]);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
            ->add('product.title')
            ->add('number');
    }

    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
            ->addIdentifier('product.title')
            ->addIdentifier('number');
    }
}